import * as AuthApi from '../api/auth';
import btoa from 'btoa';
import ReducerFactory from '../../utils/reducerFactory';

import toastr from 'toastr';
const reducerName = 'auth';

const initialState = {
    loading: false,
    user: {},
    error: null
}

const reducerFactory = new ReducerFactory(reducerName, initialState);
reducerFactory.addAction('USER_SIGNIN', 'userSignin',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await AuthApi.userSignin(data);
        return response;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        if (action.data && action.data.data && action.data.data.access_token) {
            window.localStorage.setItem('username', action.data.data.userid);
            window.localStorage.setItem('first_name', action.data.data.first_name);
            const userBtoa = btoa(JSON.stringify(action.data.data));
            window.localStorage.setItem('stories_auth',"Bearer "+ action.data.data.access_token);
            window.localStorage.setItem('user', userBtoa);
            newState.user = action.data.data;
            toastr.success('Logged in successfully');
        }
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('AUTH_USER', 'authUser',
    async () => true,
    (state) => {
        const newState = Object.assign({}, state);
        let encoded = window.localStorage.getItem('user');

        if (encoded) {
            const decoded = JSON.parse(atob(decodeURIComponent(encoded)));
            newState.user = decoded;
        }
        return newState;
    }
);

reducerFactory.addAction('USER_SIGNUP', 'userSignup',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await AuthApi.userSignup(data);
        return response;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        toastr.success(action.data.data.msg);
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('USER_LOGOUT', 'userLogout',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await AuthApi.userLogout(data);
        return response;
    }, (state) => {
        const newState = Object.assign({}, state);
        toastr.success("Logged Out sucessfully");
        newState.loading = false;
        return newState;
    }
);
export default reducerFactory;
