import * as StoriesApi from '../api/stories';
import ReducerFactory from '../../utils/reducerFactory';

import toastr from 'toastr';
const reducerName = 'post';

const initialState = {
    loading: false,
    user: {},
    storiesList: {},
    error: null
}

const reducerFactory = new ReducerFactory(reducerName, initialState);
reducerFactory.addAction('POST_LOADING', `${reducerName}Loading`,
    (status) => status, (state, action) => {
        const newState = Object.assign({}, state);
        newState.loading = action.data;
        return newState;
    }
);

reducerFactory.addAction('FETCH_STORIES', 'fetchStories',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.fetchStoriesList(data);
        return response.stories;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        newState.storiesList = action.data;
        if (action) {
            console.log('Data fetched');
        } else {
            console.log('Error');
        }
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('SEARCH_STORIES', 'searchStories',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.SearchStoriesList(data)
        return response.stories;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        newState.storiesList = action.data;
        if (action) {
            console.log('stories found');
        } else {
            console.log('Error');
        }
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('MY_STORIES', 'myStories',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.MyStoriesList(data)
        return response.stories;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        newState.storiesList = action.data;
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('SELECTED_LANGUAGE', 'selectedLanguage',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.LangList(data)
        return response.stories;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        newState.storiesList = action.data;
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('CREATE_STORY', 'createStory',
    async (data) => {
        console.log(data)
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.createStory(data);
        return response;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        if (action) {
            toastr.success('Created');
        } else {
            toastr.warning('Error');
        }       
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('UPDATE_STORY', 'updateStory',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.updateStory(data)
        return response.data;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        if (action) {
            console.log('updated');
        } else {
            console.log('Error');
        }
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('DELETE_STORY', 'deleteStory',
    async (data) => {
        // console.log(data)
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.deleteStory(data)
        console.log(data)
        return response;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        toastr.success(action.data.msg);
        newState.loading = false;
        return newState;
    }
);

reducerFactory.addAction('NEXT_PAGE', 'nextPage',
    async (data) => {
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await StoriesApi.nextPage(data);
        return response.stories;
    }, (state, action) => {
        const newState = Object.assign({}, state);
        newState.storiesList = action.data;
        if (action) {
            console.log('next page');
        } else {
            console.log('Error');
        }
        newState.loading = false;
        return newState;
    }
);


export default reducerFactory;

