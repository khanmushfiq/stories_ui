import * as PostApi from '../api/post';
import ReducerFactory from '../../utils/reducerFactory';

import toastr from 'toastr';
const reducerName = 'post';

const initialState = {
    loading: false,
    post: {},
    error: null
}

const reducerFactory = new ReducerFactory(reducerName, initialState);
reducerFactory.addAction('POST_LOADING', `${reducerName}Loading`,
    (status) => status, (state, action) => {
        const newState = Object.assign({}, state);
        newState.loading = action.data;
        return newState;
    }
);

reducerFactory.addAction('CREATE_POST', 'createPost', 
    async (data)=>{
        reducerFactory.action(`${reducerName}Loading`, true);
        const response = await PostApi.posts(data);
        return response.data;
    }, (state, action)=>{
        const newState = Object.assign({}, state);
        if(action.data.success){
            toastr.success(action.data.message);
        }else{
            toastr.warning(action.data.message);
        }
        newState.loading = false;
        return newState;
    }
);

export default reducerFactory;

