import axios from './index';
import qs from 'qs';
import config from 'appConfig';

export async function userSignin(data) {

    const newUser = {
      username: data.username,
      password: data.password,
    };
  
    const bodyData = qs.stringify(newUser);
    // debugger
    const response = await axios.post(`${config.ApiUrlStories}/users/login`, bodyData, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
    return response;
  }

export async function userSignup(data){
    return axios.post(`${config.ApiUrlStories}/users/register`, data);
}

export async function userLogout(data){
  console.log(data)
  return  axios.delete(`${config.ApiUrlStories}/users/logout`, data,{
     'accept':'application/json'
});
}
