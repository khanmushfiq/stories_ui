import config from 'appConfig';
import axios from './index';

const headerConfig = {
  headers: {
    'Content-type': 'application/json',
    // 'accept':'application/json'
  }
}

export async function fetchStoriesList(data) {
  let res = await fetch(`${config.ApiUrlStory}?&offset=0`);
  return res.json();
}

export async function SearchStory(data) {
  const newStory = { title: data.title, content: data.content };
  const body = JSON.stringify(newStory);
  let res = await fetch(`${config.ApiUrlStory}`, {
    method: 'POST',
    headerConfig,
    body,
  });
  return res.json();
}

export async function SearchStoriesList(data) {
  let res = await fetch(`${config.ApiUrlStories}/stories/?search_key=${data}&offset=0`);
  return res.json();
}

export async function MyStoriesList(data) {
  console.log(data)
  let res = await fetch(`${config.ApiUrlStories}/stories/?userid=${data}`);
  return res.json();
}

export async function LangList(data) {
  let res = await fetch(`${config.ApiUrlStories}/stories/?language=${data}`);
  return res.json();
}

export async function createStory(data) {
  debugger;
  const newStory = { 
    title: data.title, 
    content: data.content,
    language:data.language,
    category: data.category 
  };
  const body = JSON.stringify(newStory);
  // const auth = "Bearer "+ window.localStorage.stories_auth
  let response = await axios.post(`${config.ApiUrlStory}`, body);
  // window.localStorage.setItem('language', state);
// window.localStorage.setItem('language', state);
  return response.json(body);
}

export async function updateStory(data) {
  debugger;
  const newStory = { 
    title: data.title,
     content: data.content,
     language:data.language,
    category: data.category  
    };
  const body = JSON.stringify(newStory);
  let res = await axios.put(`${config.ApiUrlStory}${data.id}`,body, {
    headerConfig,
    
  });
  return res.json();
}

export async function deleteStory(data) {
  let res = await axios.delete(`${config.ApiUrlStory}${data}`, {
    headerConfig
  });
  return res.json();
}

export async function nextPage(data) {
  for (let n = 5; n < 50; n + 5) {
    let res = await fetch(`${config.ApiUrlStory}?offset=${n}`);
    // let n=5;
    // let res = await fetch(`${config.ApiUrlStory}?offset=${n}`);
    return res.json();
  }
} 