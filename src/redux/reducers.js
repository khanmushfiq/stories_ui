import { combineReducers } from 'redux-immutable';
import { connectRouter } from 'connected-react-router/immutable';
import history from 'utils/history';
import auth from 'redux/modules/auth';
import post from 'redux/modules/post';
import stories from 'redux/modules/stories';


export default function createReducer(injectedReducer = {}){
    const rootReducer = combineReducers({
        auth: auth.getReducer(),
        post: post.getReducer(),
        stories: stories.getReducer(),
        ...injectedReducer
    });
    const mergeWithRouterState = connectRouter(history);
    return mergeWithRouterState(rootReducer);
}