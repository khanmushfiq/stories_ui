const config = {
    ApiUrl: "http://localhost:5000/api/",
    ApiUrlStories: "http://20.44.193.236/lekhak",
    ApiUrlStory: "http://20.44.193.236/lekhak/stories/",
    // http://20.44.193.236/shortstory/stories?offset=10
    imageFileFormats: '.jpg, .png, .gif, .jpeg',
};

export default config;


