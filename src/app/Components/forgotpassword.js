import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import "./css/style.css";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import authReducer from 'redux/modules/auth';
import { connect } from "react-redux";





class ForgotPassword extends React.Component {

  constructor(props){
      super(props);
      this.state={
        email: 'mushfikur@gmail.com',
      }
    
    }
    handleSubmit = () =>{
      const { email } = this.state;
      const { authReducer } = this.props;
      const reqObj = { email }
      authReducer.register(reqObj).then(res=>{
        if(res && res.success){
          console.log('OTP SEND TO MAIL');
        }
      });
    }
    

render(){
  return (
    <Container component="main" maxWidth="xs" style={{height:'100vh'}}>
      <CssBaseline />
      <div className='paper'>
        <Typography component="h1" variant="h5">
          Forgot Password
        </Typography>
        <form className='form' Validate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                type="email"
                label="Email Address"
                name="email"
                autoComplete="email"
              />
            </Grid>
          </Grid>
            <Link to="/verify">
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className='submit'
                >
                    Get OTP
                </Button>
            </Link>
          <Grid container justify="flex-start">
            <Grid item>
              <Link href="/sign-up" variant="body2">
                Create A New Account, Sign up Now!
              </Link>
            </Grid>
          </Grid>
          <Grid container justify="flex-start">
            <Grid item>
              <Link href="/sign-in" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
}

const ForgotPasswordContainer = connect(
  state => ({
    error: state.get('auth').error,
    loading: state.get('auth').loading
  }),
  dispatch => ({
    authReducer: authReducer.getActions(dispatch)
  })
)(ForgotPassword)
export default (ForgotPasswordContainer)
