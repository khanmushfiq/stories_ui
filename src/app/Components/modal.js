import React from 'react'
import './css/style.css';

const Modal = ({ handleClose, show, children }) => {
  const showHideClassName = show ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName}>
      <section className="modal-main" style={{maxHeight: '500px', overflowY: 'auto'}}>
      <button type="button" onClick={handleClose} style={{float:'right', position:'sticky'}}>
          X
        </button>
        {children}
      </section>
    </div>
  );
};

export default Modal