import React from 'react';
import { connect } from "react-redux";
// import toastr from 'toastr';
import { withStyles } from '@material-ui/core/styles';
import {  Button } from '@material-ui/core';
import styles from "../styles/landingPage";
import "./css/style.css";
import PostList from './postlist'
import authReducer from '../../redux/modules/auth';
import PrimarySearchAppBar from './navbar';
import MediaCard from './catagorybox';
import { Link } from 'react-router-dom';

class Home extends React.Component {
  constructor(props){
    super(props);
    this.state = { html: " " };
    
  }

  componentWillReceiveProps(nextProps){
    if(this.props !== nextProps){
        this.props = nextProps;
    }
  }
  render() {
    return (
      <div>
        <div>
          <PrimarySearchAppBar/>
        </div>

        <div className="catagory-card">
          <MediaCard/>
        </div>
        
        <div className="postlist-area">
          <PostList/>  
        </div>
        
        <div className="browse_all">
          <Link to="/all-story">
            <Button
                    variant="contained"
                    style={{backgroundColor:'#2B6E99', color: 'white'}}
                >
                    Browse All Stories
            </Button>
          </Link>
        </div>
      </div>
    )
  }
}

const HomeContainer = connect(
  state => ({
    error: state.get('auth').error,
    loading: state.get('auth').loading
  }),
  dispatch => ({
    authReducer: authReducer.getActions(dispatch)
  })
)(Home)


export default withStyles(styles)(HomeContainer);