import React , {Component} from 'react'
import { connect } from "react-redux";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import Badge from '@material-ui/core/Badge';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import ExploreIcon from '@material-ui/icons/Explore';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import history from '../../utils/history';
import storiesRedcer from '../../redux/modules/stories';
import SearchBar from "material-ui-search-bar";
import './css/style.css';
import {browserHistory} from "react-router"


class Navbar extends Component {
  constructor(props){
      super(props)

      this.state = {
          search: "",
          posts:[],
          value: "",
          selectedLanguage: " "

      }
  }

getRes = (e) => {
  const {value} = this.state;
  const {storiesRedcer} = this.props
  console.log(value)
  
  if (value === "") {
      const {storiesRedcer} = this.props
      storiesRedcer.fetchStories()  

  } else {
      storiesRedcer.searchStories(value);
  }
}

getLang = (e) => {
  const {selectedLanguage} = this.state;
  this.setState({ selectedLanguage: e.target.value });
  const {storiesRedcer} = this.props
  console.log(e.target.value)
  
  if (selectedLanguage === ' ') {
      const {storiesRedcer} = this.props
      storiesRedcer.fetchStories()  
   } 
   else {
    //  debugger;
    storiesRedcer.selectedLanguage(selectedLanguage)  
  }
}
onChange = (e) => {
  this.setState({search: e.target.value});
}

onLogout = () => {
  window.localStorage.removeItem('user');
  window.localStorage.removeItem('stories_auth');
  // <Redirect to="/" />;
  history.push('');
  window.location.reload();
  }

  toCreateStory = () => {
    history.push('/dashboard');
  }

  toLogIn = () => {
    history.push('/sign-in');
  }

render(){ 
  const { user } = this.props;
  // const {selectedLanguage} = this.state
  return (
      <AppBar className="appbar" style={{backgroundColor:'#2B6E99'}}>
        <Toolbar className='toolbar'>
          <div className='brand'>
              <Typography variant="h6" noWrap>
                <Link to='/'><div className='brand-name'>Lekhak</div></Link>
              </Typography>
          </div>
  
          <div className='search'>
            <span className='searchbar'>
              <SearchBar
                value={this.state.value}
                onChange={(newValue) => this.setState({ value: newValue })}
                onRequestSearch = {this.getRes}
              />
             
            </span>
          </div>
          
          <div className='sectionDesktop'>
            <span className='right-part'>
              <div className="lang-selection">
                <div className="lang-conent-area">
                    {/* <div className="language-header">Select A Language To Continue</div> */}
                    <div class="lang-div">
                    <select className="language-list" class="btn btn-warning dropdown-toggle"  onChange={(e) => {
                        this.getLang(e)
                    }}>
                            <option className="languageValue" value="select" >Select Language</option>
                            <option className="languageValue" value="english" >English</option>
                            <option className="languageValue" value="hindi" >Hindi</option>
                            <option className="languageValue" value="telugu">Telugu</option>
                            <option className="languageValue" value="odia">Odia</option>
                            <option className="languageValue" value="kannada">Kannada</option>
                            <option className="languageValue" value="tamil">Tamil</option>
                            <option className="languageValue" value="marathi">Marathi</option>
                            <option className="languageValue" value="bengali">Bengali</option>
                            <option className="languageValue" value="assamese">Assamese</option>
                            <option className="languageValue" value="gujarati">Gujarati</option>
                            <option className="languageValue" value="punjabi">Punjabi</option>
                            <option className="languageValue" value="malayalam">Malayalam</option>
                        </select>
                    </div>
                </div>
            </div>
              <span className='home-btn'>
                <IconButton aria-label="Home" >
                  <Badge badgeContent={0} >
                    <Link to="/home"><HomeIcon className='homeicon' style={{color:'white'}}/></Link>
                  </Badge>
                </IconButton>
              </span>
              <span className='explore-btn'>
                <IconButton aria-label="Home" >
                  <Badge badgeContent={0} >
                    <Link to="/all-story"><ExploreIcon className='exploreicon' style={{color:'white'}}/></Link>
                  </Badge>
                </IconButton>
              </span>
              <span className='story-btn'>
                {
                  user && !user.access_token && (     
                        <Button
                            variant="contained"
                            onClick={this.toLogIn}
                            style={{ marginRight: '10px'}}
                            className='button1'
                            style={{ backgroundColor: '#EF4438', color:'white'}}
                            endIcon={<Icon>pen</Icon>}
                        >
                            Tell A Tale
                        </Button>
                  )
                }
                {
                  user && user.access_token && (    
                        <Button
                            variant="contained"
                            onClick={this.toCreateStory}
                            style={{ marginRight: '10px' }}
                            className='button1'
                            style={{ backgroundColor: '#EF4438', color:'white'}}
                            endIcon={<Icon>pen</Icon>}
                        >
                            Tell A Tale
                        </Button>
                  )
                }
                {
                  user && user.active && (
                    <Button
                        variant="contained"
                        color="secondary"
                        className='button1' 
                        style={{ backgroundColor: '#EF4438', color:'white'}}
                        onClick={this.onLogout}
                    >
                      Log out
                    </Button>
                  )
                }
              </span>
            </span>
          </div>
          
        </Toolbar>
      </AppBar>
  );
}
}
const NavbarContainer = connect(
  state => ({
      error: state.get('auth').error,
      loading: state.get('auth').loading,
      storiesList: state.get('stories').storiesList,
      user: state.get('auth').user
  }),
  dispatch => ({
      storiesRedcer: storiesRedcer.getActions(dispatch)
  })
)(Navbar)


export default NavbarContainer;

