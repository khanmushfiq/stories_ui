import React , {Component} from 'react'
import { connect } from "react-redux";
import {
  Card, CardContent, CardActionArea, Typography, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide,
} from '@material-ui/core';
import storiesRedcer from '../../redux/modules/stories';
import './css/style.css'
import config from '../../appConfig'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class Cards extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      selectedStory: {},
      openReadMore: false,
      openShare: false,
    };
    this.showModal = this.showModal.bind(this);
  }

  showModal = (e,story) => {
    this.setState({ selectedStory:story, show: true ,openReadMore: true});
  };

  onOpenShare = (e,story) => {
    this.setState({ selectedStory:story, show: true ,openShare: true});
  };

  handleCloseReadMore = () => {
    this.setState({
      openReadMore: false,
      selectedStory: {}
    });
  };

  handleCloseShare = () => {
    this.setState({
      openShare: false,
      selectedStory: {}
    });
  };

  componentDidMount(){
    const {storiesRedcer} = this.props
    storiesRedcer.fetchStories();
//     storiesRedcer.nextPage();
}

  render(){ 
    const {storiesList} = this.props
    const {selectedStory,openReadMore, openShare} = this.state
    return (
      <div style={{ padding: '2em' }}>
        {storiesList && storiesList.length > 0 && storiesList.map(story => [
          <div className="cardholder">
            <Card style={{
                    minHeight: '200px',
                    width: '90%',
                    boxShadow: '0px 0px 20px -1px rgba(0 0 0 20%)',
                    borderRadius: '15px',}}>
              <CardActionArea>
                <CardContent style={{minHeight: '150px', maxHeight: '150px', overflow:'hidden'}} className='contentarea'>
                      <Typography gutterBottom variant="h5" component="h2" style={{textTransform: 'capitalize'}}>
                      {story.title}
                    </Typography>
                    {/* <Typography variant="body2" color="textSecondary" component="p">The background of an application resembles the flat, opaque texture of a sheet of paper, and an application’s behavior mimics paper’s ability to be re-sized, shuffled, and bound together in multiple sheets.</Typography>
                    <Typography variant="body2" color="textSecondary" component="p">The background of an application resembles the flat, opaque texture of a sheet of paper, and an application’s behavior mimics paper’s ability to be re-sized, shuffled, and bound together in multiple sheets.</Typography> */}
                    <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{__html:story.content}} ></Typography>            
                </CardContent>
              </CardActionArea>
               
                <div className="btndiv">
                  {/* <Button size="small" style={{color:"#F26B61"}}>
                    Like
                  </Button>
                  <Button size="small" style={{color:"#F26B61"}} onClick={e => this.onOpenShare(e,story)}>
                    Share
                  </Button>
                  <Button size="small" style={{color:"#F26B61"}}>
                    Follow
                  </Button> */}
                  <Button size="small" style={{color:"#F26B61", float:"right"}} onClick={e => this.showModal(e,story)}>
                    Read More
                  </Button>
                </div>
            </Card>
          </div>
        ])}
        <div>
          <Dialog
            open={openReadMore}
            TransitionComponent={Transition}
            keepMounted
            maxWidth="md"
            fullWidth
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">{selectedStory && selectedStory.title}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-slide-description">
              <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{ __html: selectedStory.content }} ></Typography>
              </DialogContentText>
              <DialogContentText id="alert-dialog-slide-description">
              <Typography variant="body2" color="textSecondary" component="p">{selectedStory && selectedStory.language}</Typography>
              <Typography variant="body2" color="textSecondary" component="p">{selectedStory && selectedStory.category}</Typography>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button variant="outlined" onClick={this.handleCloseReadMore} color="secondary">
                Close
              </Button>
            </DialogActions>
          </Dialog>
        </div>
        <div>
          <Dialog
            open={openShare}
            TransitionComponent={Transition}
            keepMounted
            maxWidth="md"
            fullWidth
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">{config.ApiUrlStory}{selectedStory.id}</DialogTitle>
            <DialogActions>
              <Button variant="outlined" onClick={this.handleCloseShare} color="secondary">
                Close
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    );
  }
}

const CardsContainer = connect(
  state => ({
      error: state.get('auth').error,
      loading: state.get('auth').loading,
      storiesList: state.get('stories').storiesList
  }),
  dispatch => ({
      storiesRedcer: storiesRedcer.getActions(dispatch)
  })
)(Cards)


export default CardsContainer;
