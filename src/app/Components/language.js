import React , {Component} from 'react'
import { Link } from "react-router-dom"
import './css/style.css'

class Language extends Component {
    render(){
        return(
            <div className="laguage-selection">
                <div className="language-conent-area">
                    <div className="language-header">Select A Language To Continue</div>
                    <div class="language-div">
                    <Link to='/home'><select className="language-list" class="btn btn-warning dropdown-toggle"  >
                            <option className="languageValue" value="english">English</option>
                            <option className="languageValue" value="hindi">Hindi</option>
                            <option className="languageValue" value="telugu">Telugu</option>
                            <option className="languageValue" value="odia">Odia</option>
                            <option className="languageValue" value="kannada">Kannada</option>
                            <option className="languageValue" value="tamil">Tamil</option>
                            <option className="languageValue" value="marathi">Marathi</option>
                            <option className="languageValue" value="bengali">Bengali</option>
                            <option className="languageValue" value="assamese">Assamese</option>
                            <option className="languageValue" value="gujarati">Gujarati</option>
                            <option className="languageValue" value="punjabi">Punjabi</option>
                            <option className="languageValue" value="malayalam">Malayalam</option>
                        </select></Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Language