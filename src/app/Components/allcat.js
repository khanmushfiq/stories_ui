import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import PrimarySearchAppBar from '../Components/navbar';

const useStyles = makeStyles({
  root: {
    maxWidth: "100%",
    textAlign: "center",
  },
  card:{
    maxWidth: "15%",
    padding: "10px",
    margin: "15px",
    display: "inline-block",
    border: "2px solid #2B6E99"
  },
  media: {
    height: "150px"
  },
  all_cat_btn: {
    margin: "10px"
    
  }
});

export default function AllCatagory() {
  const classes = useStyles();

  return (
    <div className={classes.root}>

      <div>
        <PrimarySearchAppBar/>
      </div>
      
        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 1
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 2
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 3
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 4
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 5
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 6
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 7
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 8
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 9
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 10
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 11
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 12
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 13
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 14
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 15
        </CardActionArea>
        
    </div>
  );
}
