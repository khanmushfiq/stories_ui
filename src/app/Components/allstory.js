import React , {Component} from 'react'
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import styles from "app/styles/landingPage.js";
import storiesRedcer from '../../redux/modules/stories';
import RecipeReviewCard from '../Components/cards';
import PrimarySearchAppBar from '../Components/navbar';
import "./css/style.css"

class AllStory extends Component {
    constructor(props){
        super(props)

        this.state = {
            search: "",
            posts:[]

        }
    }
    componentDidMount(){
        const {storiesRedcer} = this.props
        storiesRedcer.nextPage();
        storiesRedcer.fetchStories();
    }
    
    load = (e) =>{
        const {storiesRedcer} = this.props
        storiesRedcer.nextPage(e);
        console.log(storiesRedcer.nextPage)
        
    }
    getRes = (e) => {
        const {search} = this.state;
        const {storiesRedcer} = this.props
        
        if (search === "") {
            const {storiesRedcer} = this.props
            storiesRedcer.fetchStories();
            storiesRedcer.nextPage();
        } else {
            storiesRedcer.searchStories(search);
        }
    }

    onChange = (e) => {
            this.setState({search: e.target.value});
    }

    render(){
        // const {storiesList} = this.props
        return(
            <div className="main_div">
                <div>
                    <PrimarySearchAppBar />
                </div>

                <div className="cards">
                    <RecipeReviewCard/>
                </div>

                <div>
                    <button onClick={e=> this.load(e)}> Load More</button>
                </div>
            </div>
        )
    }
}

const PostlistContainer = connect(
    state => ({
        error: state.get('auth').error,
        loading: state.get('auth').loading,
        storiesList: state.get('stories').storiesList
    }),
    dispatch => ({
        storiesRedcer: storiesRedcer.getActions(dispatch)
    })
)(AllStory)


export default withStyles(styles)(PostlistContainer);