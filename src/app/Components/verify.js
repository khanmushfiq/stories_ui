import React from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import "./css/style.css";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import authReducer from 'redux/modules/auth';
import { connect } from "react-redux";





class Verify extends React.Component {

  constructor(props){
      super(props);
      this.state={
        OTP: '',
      }
    
    }
    handleSubmit = () =>{
      const { OTP } = this.state;
      const { authReducer } = this.props;
      const reqObj = { OTP }
      authReducer.register(reqObj).then(res=>{
        if(res && res.success){
          console.log('OTP Verified');
        }
      });
    }
    

render(){
  return (
    <Container component="main" maxWidth="xs" style={{height:'100vh'}}>
      <CssBaseline />
      <div className='paper'>
        <Typography component="h1" variant="h5">
          Verify OTP
        </Typography>
        <form className='form' Validate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="OTP"
                type="OTP"
                label="Enter OTP"
                name="OTP"
                autoComplete="OTP"
              />
            </Grid>
          </Grid>
            <Link to="/verify">
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className='submit'
                >
                    Verify
                </Button>
            </Link>
        </form>
      </div>
    </Container>
  );
}
}

const VerifyContainer = connect(
  state => ({
    error: state.get('auth').error,
    loading: state.get('auth').loading
  }),
  dispatch => ({
    authReducer: authReducer.getActions(dispatch)
  })
)(Verify)
export default (VerifyContainer)
