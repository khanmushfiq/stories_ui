import React, { Component } from 'react';
import { connect } from "react-redux";
import storiesRedcer from '../../redux/modules/stories';
import styles from "app/styles/landingPage.js";
import { withStyles } from '@material-ui/core/styles';
import { Card, CardContent, CardActionArea, TextField, Typography, Button, Grid, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide, CardActions} from '@material-ui/core';
import { Link } from 'react-router-dom';
import Modal from './modal.js';
import './css/style.css'
import NavBar from './navbar';
import history from '../../utils/history';
import PersonalCardsContainer from './personalcards';
import PostFormContainer from './postform';
import reducerFactory from '../../redux/modules/auth';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

class Dashboard extends Component {

    onLogout = () => {
        // reducerFactory.userLogout()
    window.localStorage.removeItem('stories_auth');
    window.localStorage.removeItem('user');
    history.push('/home');
    window.location.reload();
    }
    constructor() {
        super();
        this.state = {
            openCreate: false,
        };
      }
    
      onOpenCreate = (e, story) => {
        this.setState({ selectedStory: story, openCreate: true });
      }; 

      closeCreateDialog = () => {
        this.setState({ openCreate: false, selectedStory: {} });
        window.location.reload();
      }
    
    render() {
        const {  openCreate } = this.state;
        return (
            <div className='dashboard'>
                <NavBar />
                <div className='left'>
                    <div className='welcome-div'>
                        <h1 className='welcome'>Welcome</h1>
                    </div>
                    <div className='nametag-div'>
                        <span className='nametag'>{window.localStorage.first_name}</span>
                    </div>
                    <div className='dashboard-btn-div'>
                        <Button className='dashboard-btn' style={{backgroundColor:'#FFB85F'}}>Profile</Button>
                    </div>
                    <div className='dashboard-btn-div'>
                        <Button className='dashboard-btn' style={{backgroundColor:'#4AC660'}} onClick={e => this.onOpenCreate()}>Create Story</Button>
                    </div>
                    <div className='dashboard-btn-div'>
                        <Link to="/home"><Button className='dashboard-btn' onClick={this.onLogout} style={{backgroundColor:'#F26B61'}}>Log Out</Button></Link>
                    </div>
                    <div className='dashboard-btn-div'>
                        <Link to="/setting"><Button className='dashboard-btn'  style={{backgroundColor:'#764CB2'}}>Settings</Button></Link>
                    </div>
                </div>
                
                <div className='right'>
                    <PersonalCardsContainer/>
                </div>

                <div>
                    <Dialog
                        open={openCreate}
                        TransitionComponent={Transition}
                        keepMounted
                        maxWidth="md"
                        fullWidth
                        onClose={this.handleClose}
                        aria-labelledby="alert-dialog-slide-title"
                        aria-describedby="alert-dialog-slide-description"
                    >
                        <PostFormContainer/>
                        <DialogActions>
                            <div style={{ marginTop: '1em', float: 'right' }}>
                                <Button style={{ marginRight: '1em' }} onClick={this.closeCreateDialog} color="secondary" variant="outlined">Cancel</Button>
                            </div>
                        </DialogActions>
                    </Dialog>
                </div>
            </div>
        )
    }
}

const DashboardContainer = connect(
    state => ({
        error: state.get('auth').error,
        loading: state.get('auth').loading
    }),
    dispatch => ({
        storiesRedcer: storiesRedcer.getActions(dispatch)
    })
)(Dashboard)


export default withStyles(styles)(DashboardContainer);

