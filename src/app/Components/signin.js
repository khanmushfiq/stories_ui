import React from 'react';
import Link from '@material-ui/core/Link';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import authReducer from '../../redux/modules/auth';
import { connect } from "react-redux";
import history from "../../utils/history";
import "./css/style.css";

class SignIn extends React.Component {

constructor(props){
    super(props);
    this.state={
      username: '',
      password: '',
    }
  
  }

  handleSubmit = (e) =>{

    e.preventDefault();
    const { username, password} = this.state;
    const { authReducer } = this.props;
    const reqObj = {username, password}
    // debugger;
    authReducer.userSignin(reqObj).then(res=>{
      debugger;
      // console.log(res)
      this.setState({ username: '', password: '' });
     
      if(res && res.data && res.data.access_token){
        console.log(res.data.access_token)
        console.log('User loggedin');
        history.push('/dashboard')
      } else {
        console.log('Wrong Credentials')
      }
    });
  }
  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
};


render(){
  const{username,password} =this.state
  return (
    <div style={{height:'100vh'}}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className="paper">
          <Avatar className="avatar">
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className="form" Validate onSubmit={this.handleSubmit}>
            <TextField
              variant="outlined"
              color="secondary"
              margin="normal"
              required
              fullWidth
              id="email"
              type="email"
              label="Email Address"
              name="username"
              value={username}
              onChange={this.changeHandler}
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              onChange={this.changeHandler}
              value={password}
              autoComplete="current-password"
            />
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
            <Link href="/dashboard"><Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className="submit"
            >
              Sign In
            </Button></Link>
            <Grid container>
              <Grid item xs>
                <Link href="/forgot-password" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/sign-up" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>
    </div>
  );
}
}

const SignInContainer = connect(
  state => ({
    error: state.get('auth').error,
    loading: state.get('auth').loading,
    active: state.get('auth').active
  }),
  dispatch => ({
    authReducer: authReducer.getActions(dispatch)
  })
)(SignIn)
export default (SignInContainer)