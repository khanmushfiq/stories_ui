import React, { Component } from 'react';
import { connect } from "react-redux";
import toastr from "toastr";
import storiesReducer from '../../redux/modules/stories';
import { withStyles } from '@material-ui/core/styles';
import styles from "app/styles/landingPage.js";
import sanitizeHtml from "sanitize-html";
import { TextField, Typography, Button } from '@material-ui/core';
import ReactQuill from 'react-quill'; // ES6
import 'react-quill/dist/quill.snow.css';



class PostForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            html: '',
            editable: true,
            title: '',
            text: '',
            language: '',
            category: '',
            // editorState: EditorState.createEmpty()
        }
        this.handleChange = this.handleChange.bind(this)
        // this.onChange = editorState => this.setState({editorState});
        // this.handleKeyCommand = this.handleKeyCommand.bind(this);
    };

    modules = {
        toolbar: [
            [{ 'font': [] }],
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }, { 'indent': '-1' }, { 'indent': '+1' }],
            [{ 'color': [] }, { 'background': [] }],
            ['image'],
        ],
    }

    formats = [
        'font',
        'header',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'color', 'background',
        'image'
    ]

    handleChange(value) {
        this.setState({ text: value })
        console.log(value)
    }

    pageReload = () => {
        window.location.reload();
    };
    sanitizeConf = {
        allowedTags: ["b", "i", "em", "strong", "a", "p", "h1"],
        allowedAttributes: { a: ["href"] }
    };

    sanitize = () => {
        this.setState({ html: sanitizeHtml(this.state.html, this.sanitizeConf) });
    };

    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };
    changeLanguage = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };
    changecategory = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };
    

    submitHandler = e => {
        e.preventDefault()
        const { title, text, language, category } = this.state;
        const { storiesReducer } = this.props;

        const reqObj = { title: title, language: language, category: category, content: text };
        if (text === "") {
            return toastr.warning("Story Title & Content Is Required!!");
        }
        storiesReducer.createStory(reqObj).then(res => {
            this.setState({ title: '', language:'',category:'', text: '' });
        });
    };

    render() {
        const { title, language, category } = this.state;
        return (
            <React.Fragment>
                {/* <NavBar /> */}
                <div style={{ textAlign: "center", position: 'relative', alignItems: 'center', marginTop: '10%' }}>
                    <form onSubmit={this.submitHandler}>
                        <div>
                            <Typography variant="body2" gutterBottom>
                                Enter story title
                            </Typography>
                            <TextField
                                style={{ width: '80%' }}
                                onChange={this.changeHandler}
                                id="outlined-basic"
                                label="Story title"
                                variant="outlined"
                                name="title"
                                value={title}
                                required
                            />
                        </div>
                        <div>
                            <Typography variant="body2" gutterBottom>
                                Select Language
                            </Typography>
                            <TextField
                                style={{ width: '80%' }}
                                onChange={this.changeLanguage}
                                id="outlined-basic"
                                label="Story Language"
                                variant="outlined"
                                name="language"
                                value={language}
                                required
                            />
                        </div>
                        <div>
                            <Typography variant="body2" gutterBottom>
                                Select category
                            </Typography>
                            <TextField
                                style={{ width: '80%' }}
                                onChange={this.changecategory}
                                id="outlined-basic"
                                label="Story category"
                                variant="outlined"
                                name="category"
                                value={category}
                                required
                            />
                        </div>
                        <div style={{ textAlign: 'center', height: '500px' }}>
                            <Typography variant="body2" gutterBottom>
                                Enter story content
                            </Typography>
                            <ReactQuill theme="snow"
                                modules={this.modules}
                                formats={this.formats}
                                value={this.state.text}
                                onChange={this.handleChange}
                                style={{ marginLeft: '10%', width: '80%', height: '350px' }}
                            />

                        </div>

                        <div>
                            <Button type="submit" variant="outlined" >Create</Button>
                            {/* onSubmit= {this.pageReload()} */}
                        </div>
                    </form>
                </div>
            </React.Fragment>
        )
    }
}

const PostFormContainer = connect(
    state => ({
        error: state.get('stories').error,
        loading: state.get('stories').loading
    }),
    dispatch => ({
        storiesReducer: storiesReducer.getActions(dispatch)
    })
)(PostForm)


export default withStyles(styles)(PostFormContainer);

