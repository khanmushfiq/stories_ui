// import { ViewHeadline } from '@material-ui/icons'
import React , {Component} from 'react'
import { Link } from "react-router-dom"
import './css/style.css'

class NotFoundPage extends Component {
    render(){
        return(
            <div style={{color:'#e0e2f4', background: '#0414a7', height: '100vh'}}>
                <main class="bsod container">
                    <h1 class="neg title head1"><span class="bg">Error - 404</span></h1>
                    <h3 class="neg title head3"><span class="bg">Page Not Fonud</span></h3>
                    <p class='para'>Page Not Fonud</p>
                    <p class='para'>An error has occured, to continue:</p>
                    <p class='para'>* Return to our homepage.<br />
                    * Send us an e-mail about this error and try again later.</p>
                    <nav class="nav">
                        <Link to='/' class="links">Home</Link>&nbsp;|&nbsp;<Link to='/all-story' class="links">Story</Link>
                    </nav>
                </main>
            </div>
        )
    }
}

export default NotFoundPage