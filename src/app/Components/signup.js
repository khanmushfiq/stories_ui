import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import "./css/style.css";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import authReducer from '../../redux/modules/auth';
import { connect } from "react-redux";
import history from "../../utils/history";
import toastr from "toastr"

class SignUp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      userid: '',
      first_name: '',
      last_name: '',
      image: '',
      sessionid: '',
      email: '',
      password: '',
      confirmed_password: ''
    }

  }
  handleSubmit = (e) => {
    e.preventDefault();

    const { first_name, last_name, email, password, confirmed_password } = this.state;
    const { authReducer } = this.props;

    const reqObj = { userid: email, first_name, last_name, email, password, confirmed_password };
    if (first_name === "") {
      return toastr.warning("First Name Is Required!!");
    }
    // debugger
    authReducer.userSignup(reqObj).then(res => {
      this.setState({ first_name: '', last_name: '', email: '', password: '', confirmed_password: '' });
        history.push('/sign-in');
    });
  }

  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { first_name, last_name, email, password, confirmed_password } = this.state
    return (
      <div style={{ height: '100vh' }}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className='paper'>
            <Avatar className='avatar'>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign up
          </Typography>
            <form className='form' Validate onSubmit={this.handleSubmit}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="first_name"
                    name="first_name"
                    variant="outlined"
                    required
                    fullWidth
                    id="first_name"
                    onChange={this.changeHandler}
                    value={first_name}
                    label="first_name"
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="last_name"
                    onChange={this.changeHandler}
                    value={last_name}
                    label="Last Name"
                    name="last_name"
                    autoComplete="last_name"
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="email"
                    onChange={this.changeHandler}
                    value={email}
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    onChange={this.changeHandler}
                    value={password}
                    autoComplete="current-password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    name="confirmed_password"
                    label="confirmed_Password"
                    type="password"
                    id="confirmed_password"
                    onChange={this.changeHandler}
                    value={confirmed_password}
                    autoComplete="confirmed_password"
                  />
                </Grid>
                <Grid item xs={12}>
                  {/* <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" color="primary" />}
                  label="I want to receive inspiration, marketing promotions and updates via email."
                /> */}
                </Grid>
              </Grid>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className='submit'
                onSubmit={this.handleSubmit}

              >
                Sign Up
            </Button>
              <Grid container justify="flex-end">
                <Grid item>
                  <Link href="/sign-in" variant="body2">
                    Already have an account? Sign in
                </Link>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>
      </div>
    );
  }
}
const SignUpContainer = connect(
  state => ({
    error: state.get('auth').error,
    loading: state.get('auth').loading,
    active: state.get('auth').active
  }),
  dispatch => ({
    authReducer: authReducer.getActions(dispatch)
  })
)(SignUp)
export default (SignUpContainer)
