import React , {Component} from 'react'
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import styles from "app/styles/landingPage.js";
import storiesRedcer from '../../redux/modules/stories';
import {Typography } from '@material-ui/core';
import "./css/style.css"
import CardsContainer from '../Components/cards';
// import CustomizedInputBase from './searchbar';


class PostList extends Component {
    constructor(props){
        super(props)

        this.state = {
            search: "",
            posts:[]

        }
    }
    render(){
        // const {storiesList} = this.props
        return(
            <div className="main_div">
                 <div className="rcnt_stry">
                    <Typography>
                        <h1>Recent Stories</h1>
                    </Typography>
                </div>
                <div className="cards">
                    <CardsContainer/>
                </div>
            </div>

        )
    }
}

// <span className="title">{story.title}</span>
// <span className="content" dangerouslySetInnerHTML={{__html:story.content}}></span>

const PostlistContainer = connect(
    state => ({
        error: state.get('auth').error,
        loading: state.get('auth').loading,
        storiesList: state.get('stories').storiesList
    }),
    dispatch => ({
        storiesRedcer: storiesRedcer.getActions(dispatch)
    })
)(PostList)


export default withStyles(styles)(PostlistContainer);