// import React, { Component } from 'react';
// import { connect } from "react-redux";
// import toastr from "toastr";
// import storiesRedcer from '../../redux/modules/stories';
// import ReactQuill from 'react-quill'; // ES6
// import 'react-quill/dist/quill.snow.css';
// import PrimarySearchAppBar from '../Components/navbar';
// import { TextField, Typography, Button } from '@material-ui/core';


// class Update extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             title: '',
//             text: ''
//         }
//         this.handleChange = this.handleChange.bind(this)

//     }
//     modules = {
//         toolbar: [
//             [{ 'font': [] }],
//           [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
//           ['bold', 'italic', 'underline','strike', 'blockquote'],
//           [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
//           [{ 'color': [] }, { 'background': [] }], 
//           ['image'],
//         ],
//       }
     
//       formats = [
//         'font',
//         'header',
//         'bold', 'italic', 'underline', 'strike', 'blockquote',
//         'list', 'bullet', 'indent',
//         'color', 'background',
//         'image'
//       ]
//       handleChange(value) {
//         this.setState({ text: value })
//       };
//       changeHandler = (e) => {
//         this.setState({ [e.target.name]: e.target.value });
//     };

//       submitHandler = e => {
//         e.preventDefault()
//         const { title, text } = this.state;
//         const { storiesRedcer } = this.props;
        
//         const reqObj = { title: title, content: text };
//         if (text === "") {
//             return toastr.warning("Story Content Is Required!!");
//         }
//         storiesRedcer.updateStories(reqObj).then(res => {
//             this.setState({ title: '', text: '' });
//         });
//     };
//     render() {
//         const { title } = this.state;
//         return (
//             <React.Fragment>
//             <PrimarySearchAppBar/>
//             <div style={{textAlign: "center", position: 'relative', alignItems: 'center', marginTop: '10%'}}>
//                 <form onSubmit={this.submitHandler}>
//                     <div>
//                         <Typography variant="body2" gutterBottom>
//                             Enter story title
//                         </Typography>
//                         <TextField
//                             style={{ width: '50%' }}
//                             onChange={this.changeHandler}
//                             id="outlined-basic"
//                             label= {this.title}
//                             variant="outlined"
//                             value={title}
//                             name="title"
//                             required
//                         />
//                     </div>
//                     <div style={{textAlign:'center'}}>
//                         <Typography variant="body2" gutterBottom>
//                             Enter story content
//                         </Typography>
//                         <ReactQuill theme = "snow"
//                                     modules={this.modules}
//                                     formats={this.formats}
//                                     value={this.state.text}
//                                     onChange={this.handleChange}
//                                     style={{marginLeft:'25%', width:'50%'}}
//                         />
//                 </div>
//                 <div>
//                     <Button type="submit" variant="outlined">Update</Button>
//                 </div>
//             </form>
//         </div>
//     </React.Fragment>

//         )
// }
// }
// const UpdateContainer = connect(
//     state => ({
//         error: state.get('auth').error,
//         loading: state.get('auth').loading
//     }),
//     dispatch => ({
//         storiesRedcer: storiesRedcer.getActions(dispatch)
//     })
// )(Update)


// export default (UpdateContainer);