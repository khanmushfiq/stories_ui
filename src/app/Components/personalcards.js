import React, { Component } from 'react'
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import storiesRedcer from '../../redux/modules/stories';
import {
  Card, CardContent, CardActionArea, TextField, Typography, Button, Grid, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Slide, CardActions
} from '@material-ui/core';
import ReactQuill from 'react-quill'; // ES6
import sanitizeHtml from "sanitize-html";
import './css/style.css'

import styles from "../styles/card-jss";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class PersonalCards extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      dummy: 'hello',
      selectedStory: {},
      storyId: '',
      title: '',
      content:'',
      language:'',
      category:'',
      deleteOpen: false,
      openDialog: false,
      openReadMore: false,
      openEdit: false,
      openDelete: false,
      userProspectiveStories:'',
      loggedInUser: '',
    };
    this.showModal = this.showModal.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.changeHandler = this.changeHandler.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.props = nextProps;
    }
  }

  showModal = (e, story) => {
    this.setState({ selectedStory: story, openReadMore: true });
  };

  onOpenEdit = (e, story) => {
    this.setState({ selectedStory: story, openEdit: true });
  };

  onOpenDelete = (e, story) => {
    console.log(story)
    this.setState({ selectedStory: story, openDelete: true });
  };  

  hideModal = () => {
    this.setState({ show: false });
  };

  componentDidMount() {
    const { storiesRedcer, user } = this.props
    if (user && user.length > 0 ) {
      {this.setState({loggedInUser: user.userid})}
    }
    storiesRedcer.fetchStories();
  }
  handleChange = (value) => {
    this.setState({ text: value })
  }

  changeHandler = (e) => {
    const { selectedStory } = this.state;
    let newStory = Object.assign({}, selectedStory);
    newStory.title = e.target.value;
    this.setState({ selectedStory: newStory });
  };

  changelanguage = (e) => {
    const { selectedStory } = this.state;
    let newStory = Object.assign({}, selectedStory);
    newStory.language = e.target.value;
    this.setState({ selectedStory: newStory });
  };

  changecategory = (e) => {
    const { selectedStory } = this.state;
    let newStory = Object.assign({}, selectedStory);
    newStory.category = e.target.value;
    this.setState({ selectedStory: newStory });
  };

  sanitizeConf = {
    allowedTags: ["b", "i", "em", "strong", "a", "p", "h1"],
    allowedAttributes: { a: ["href"] }
  };

  sanitize = () => {
    this.setState({ html: sanitizeHtml(this.state.html, this.sanitizeConf) });
  };

  modules = {
    toolbar: [
        [{ 'font': [] }],
      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      ['bold', 'italic', 'underline','strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      [{ 'color': [] }, { 'background': [] }], 
      ['image'],
    ],
  }
 
  formats = [
    'font',
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'color', 'background',
    'image'
  ]

  submitHandler = () => {
    const { storiesRedcer } = this.props;
    const { selectedStory } = this.state;
    storiesRedcer.deleteStory(selectedStory.id).then(res => {
      this.setState({ openDelete: false, selectedStory: {} });
      storiesRedcer.fetchStories();
      window.location.reload();
      // storiesRedcer.nextPage();
    });
  }

  submitHandler1 = () => {
    const { storiesRedcer } = this.props;
    const { selectedStory } = this.state;
    const reqObj = { id:selectedStory.id,title: selectedStory.title, language: selectedStory.language, category: selectedStory.category, content: selectedStory.content };
    storiesRedcer.updateStory(reqObj).then(res => {
      this.setState({ title: '', content: '', language:'',category:'', });
      storiesRedcer.fetchStories();
      // storiesRedcer.nextPage();
    });
  }

  pageReload = () => {
    window.location.reload();
};

  handleClickOpen = () => {
    this.setState({
      openReadMore: true
    });
  };

  handleCloseReadMore = () => {
    this.setState({
      openReadMore: false,
      selectedStory: {}
    });
  };

  closeEditDialog = () => {
    this.setState({ openEdit: false, selectedStory: {} });
  }

  closeDeleteDialog = () => {
    this.setState({ openDelete: false, selectedStory: {} });
    // window.location.reload();
  }

  render() {
    const { classes, storiesList } = this.props;
    const { openReadMore, openEdit, selectedStory, openDelete } = this.state;
    // console.log(this.state.loggedInUser)
    const regX = /(<([^>]+)>)/ig;
    const cleanQuillText = selectedStory && selectedStory.title && selectedStory.title !== "" ? selectedStory.content.replace(regX, "") : "hello";

    return (
      <div style={{ padding: '2em' }}>
        {
          storiesList && storiesList.length > 0 && storiesList.map((story, k) => {
            return (
              story.user.userid === window.localStorage.username && 
              <div key={k} style={{ marginBottom: '1em' }} className="cardholder">
                  <Card style={{
                          minHeight: '200px',
                          width: '90%',
                          boxShadow: '0px 0px 20px -1px rgba(0 0 0 20%)',
                          borderRadius: '15px',}}>
                     <CardContent style={{minHeight: '150px', maxHeight: '150px', overflow:'hidden'}} className='contentarea'>
                        <Typography gutterBottom variant="h5" component="h2" style={{textTransform: 'capitalize'}}>
                          {story.title}
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{__html:story.content}} ></Typography>
                    </CardContent>
                    <CardActions>
                      <Button variant="outlined" size="small" color="primary" onClick={e => this.onOpenEdit(e, story)}  >
                        Update
                      </Button>
                      <Button variant="outlined" size="small" color="primary" onClick={e => this.onOpenDelete(e, story)} >
                        Delete
                      </Button>
                      <Button variant="outlined" style={{ float: 'right' }} size="small" color="primary" onClick={e => this.showModal(e, story)}>
                        Read More
                      </Button>
                    </CardActions>
                  </Card>
            </div>
            )
          }
          )
        }
        <div>
          <Dialog
            open={openReadMore}
            TransitionComponent={Transition}
            keepMounted
            maxWidth="md"
            fullWidth
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">{selectedStory && selectedStory.title}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-slide-description">
              <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{ __html: selectedStory.content }} ></Typography>

              <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{ __html: selectedStory.language }} ></Typography>
              <Typography variant="body2" color="textSecondary" component="p" dangerouslySetInnerHTML={{ __html: selectedStory.category }} ></Typography>
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button variant="outlined" onClick={this.handleCloseReadMore} color="secondary">
                Close
              </Button>
            </DialogActions>
          </Dialog>
        </div>

        {/* ------------------------- */}
        <div>
          <Dialog
            open={openDelete}
            TransitionComponent={Transition}
            keepMounted
            maxWidth="md"
            fullWidth
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="alert-dialog-slide-title">Are you sure you want to delete this?</DialogTitle>
            <DialogActions>
                <div style={{ marginTop: '1em', float: 'right' }}>
                    <Button style={{ marginRight: '1em' }} onClick={this.closeDeleteDialog} color="secondary" variant="outlined">No</Button>
                    <Button onClick={e => this.submitHandler(e)}  variant="outlined">Yes</Button>
                </div>
            </DialogActions>
          </Dialog>
        </div>
        {/* --------------------------------- */}
        <div>
          <Dialog
            open={openEdit}
            TransitionComponent={Transition}
            keepMounted
            maxWidth="md"
            fullWidth
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogContent>
            <Card className={classes.editRoot}>
            <form onSubmit={this.submitHandler1}>
                  <div>
                    <Typography variant="body2" gutterBottom>
                      Edit story title
                              </Typography>
                    <TextField
                      style={{ width: '100%' }}
                      onChange={e => this.changeHandler(e)}
                      id="outlined-basic"
                      variant="outlined"
                      name="title"
                      value={selectedStory.title}
                      required
                    />
                  </div>

                  <div>
                    <Typography variant="body2" gutterBottom>
                      Edit story Language
                              </Typography>
                    <TextField
                      style={{ width: '100%' }}
                      onChange={e => this.changelanguage(e)}
                      id="outlined-basic"
                      variant="outlined"
                      name="title"
                      value={selectedStory.language}
                      required
                    />
                  </div>

                  <div>
                    <Typography variant="body2" gutterBottom>
                      Edit story category
                              </Typography>
                    <TextField
                      style={{ width: '100%' }}
                      onChange={e => this.changecategory(e)}
                      id="outlined-basic"
                      variant="outlined"
                      name="title"
                      value={selectedStory.category}
                      required
                    />
                  </div>
                  
                  <div style={{ marginTop: '1em' }}>
                    <Typography variant="body2" gutterBottom>
                      Edit story content
                              </Typography>
                    <ReactQuill theme="snow"
                      modules={this.modules}
                      formats={this.formats}
                      value={cleanQuillText}
                      onChange={this.handleChange}
                      style={{ width: '100%' }}
                    />
                  </div>
                  <div style={{ marginTop: '1em', float: 'right' }}>
                    <Button style={{ marginRight: '1em' }} onClick={this.closeEditDialog} color="secondary" variant="outlined">Cancel</Button>
                    <Button type="submit" variant="outlined">Update</Button>
                  </div>
                </form>
            </Card>
            </DialogContent>
          </Dialog>
        </div>
      </div>
    );
  }
}


const PersonalCardsContainer = connect(
  state => ({
    error: state.get('auth').error,
    loading: state.get('auth').loading,
    storiesList: state.get('stories').storiesList,
    user: state.get('auth').user
  }),
  dispatch => ({
    storiesRedcer: storiesRedcer.getActions(dispatch)
  })
)(PersonalCards)

export default withStyles(styles)(PersonalCardsContainer);

