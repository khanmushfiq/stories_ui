import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    maxWidth: "100%",
    textAlign: "center",
  },
  card:{
    maxWidth: "15%",
    padding: "10px",
    margin: "15px",
    display: "inline-block",
    border: "2px solid #2B6E99"
  },
  media: {
    height: "150px"
  },
  all_cat_btn: {
    margin: "10px"
    
  }
});

export default function MediaCard() {
  const classes = useStyles();

  return (
    <div>
      <Card className={classes.root}>
        
        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            title="Contemplative Reptile"
            image = "../../../public/heartbroke.png"
          />Catagory 1
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 2
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 3
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 4
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 5
        </CardActionArea>

        <CardActionArea className={classes.card}>
          <CardMedia
            className={classes.media}
            image="https://commons.wikimedia.org/wiki/File:React-icon.svg"
            title="Contemplative Reptile"
          />Catagory 6
        </CardActionArea>
        
        <div>
        <Link to="/all-catagory">
        <Button variant="contained" style={{backgroundColor:'#2B6E99', color: 'white'}} className={classes.all_cat_btn}>              Check All Catagories
            </Button>
          </Link>
        </div>
      </Card>

      
    </div>
  );
}
