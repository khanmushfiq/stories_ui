const cardStyle = theme =>({
    root: {
        minWidth: 275
      },
      editRoot: {
        minWidth: 275,
        padding: '2em'
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
      },
      pos: {
        marginBottom: 12,
      },
    });
  
  export default cardStyle;
  