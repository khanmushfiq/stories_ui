const landingPageStyle = theme =>({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  });

export default landingPageStyle;
