import React from 'react';
import atob from 'atob';
import { Switch, Route, Redirect } from 'react-router-dom';
import storiesRedcer from './redux/modules/stories';
import authReducer from './redux/modules/auth';
// import Home from 'app/Components/home';
import SignIn from './app/Components/signin'
import SignUp from './app/Components/signup'
import NotFoundPage from './app/Components/404'
import AllCatagory from 'app/Components/allcat';
import AllStory from 'app/Components/allstory';
import ForgotPassword from 'app/Components/forgotpassword';
import Verify from 'app/Components/verify';
import { connect } from "react-redux";
import dashboard from 'app/Components/dashboard';
import Language from 'app/Components/language';
import Update from 'app/Components/update';
import Home from 'app/Components/home';



class App extends React.Component{

  componentWillReceiveProps(nextProps){
    if(this.props !== nextProps){
        this.props = nextProps;
    }
  }

  componentWillMount(){
    const { authReducer } = this.props;
    authReducer.authUser();
  }

  render(){
    // const { user } = this.props;
    return(
      <div className="App">
        <Switch>
          <Route exact path="/" component={Language} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/sign-in" component={SignIn} />
          <Route exact path="/sign-up" component={SignUp} /> 
          <Route exact path="/dashboard" component={dashboard} />
          <Route exact path="/all-catagory" component={AllCatagory} />
          <Route exact path="/all-story" component={AllStory} />
          <Route exact path="/update" type="private"component={Update} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route exact path="/verify" component={Verify} />
          <Route exact path="/404" component={NotFoundPage} />
          <Redirect to="/404" />
        </Switch>
      </div>
    )
  }
}

const AppContainer = connect(
  state => ({
      error: state.get('auth').error,
      loading: state.get('auth').loading,
      user: state.get('auth').user
  }),
  dispatch => ({
    authReducer: authReducer.getActions(dispatch)
  })
)(App)


export default (AppContainer);
